param(
	[String[]] $Folders
)

$site = 'JOBs/DS/Quran'
$local = (Join-Path $PSScriptRoot 'dist')
$remote = '/httpdocs'

[Console]::Write("`ec")

rmr ./dist/*,./dist/,./node_modules/.cache/*

yarn build

if($lastExitCode -ne 0) {
	Write-Error 'Build Error'
	exit $lastExitCode
}

if($Folders){
	foreach($folder in $Folders){
		if (Test-Path ($Here = Join-Path $local $folder)) {
			$command = "synchronize remote $Here $($remote,$folder -join '/') -delete -mirror\n"
		}
	}
} else {
	$command = "synchronize remote $local $remote -delete -mirror\n"
}
$command += "exit"

winscp.com $site /command $command

$winscpResult = $LastExitCode
if ($winscpResult -eq 0) {
  'Delivery Success'
} else {
  Write-Error 'Delivery Error'
}

exit $winscpResult
