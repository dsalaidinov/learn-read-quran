<#
	Clear background rects in btn icons
#>

param(
	[int] $DaysOld = 1,
	[String] $PatternToRemove = '<rect(?:\s+id="bound-bg")?[^>]*/>|</?g[^>]*>',
	[Switch] $GitModifiedOnly = $False
)

[String] $BtnIconsPath = './static/img/btns/*.svg'

$FileList = $GitModifiedOnly ?
	[String[]](git status -s -- $BtnIconsPath | ForEach-Object {$_ -replace '^\s*M\s*','.\'}) :
	[String]$BtnIconsPath

$Recurse = 'String' -eq $FileList.GetType()

$CountChanges = 0
$CountTotal = 0

function p([String[]]$s){[Console]::Write($s -Join '')}
function pn([String[]]$s){[Console]::WriteLine($s -Join '')}
function pc([String]$s,[String]$c=' '){p `r,($c*[Console]::WindowWidth),`r,$s}

git add -- $BtnIconsPath

Get-ChildItem -Path $FileList -Recurse:$Recurse |
	Where-Object LastWriteTime -gt (Get-Date).AddDays(-$DaysOld) |
		Foreach-Object {
			$FileName = $_.FullName
			$Content = [String](Get-Content $FileName)

			pc ("`r{0} [ {1:n1}Kb ] - `e[33m{2}`e[0m / {3}" -f $FileName,
				($Content.Length/1kb),
				$CountChanges,$CountTotal++)

			if($Content -Match $PatternToRemove){

				$CountChanges++

				$Content -Replace $PatternToRemove,'' `
					-Replace '\s+id="[^"]*"\s+',' ' `
						-Replace '(?<=>)\s+(?=<)',"`n" |
							Set-Content -Path $FileName
			}
		}

pc "Files updated: `e[33m$CountChanges`e[0m/`e[36m$CountTotal`e[0m`n"
