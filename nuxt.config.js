import path from 'path'
import fs from 'fs'

export default {
	ssr: false,
	/*
	 ** Headers of the page
	 */
	server: {
		https: {
			key: fs.readFileSync(path.resolve(__dirname, 'nuxt.local+4-key.pem')),
			cert: fs.readFileSync(path.resolve(__dirname, 'nuxt.local+4.pem')),
		},
	},
	head: {
		title: 'Құран әліппесі',
		htmlAttrs: {
			lang: 'kz',
		},
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || '',
			},
			{ name: 'msapplication-TileColor', content: '#303275' },
			{
				name: 'msapplication-config',
				content: '/img/favicon/browserconfig.xml?v=dLXj2jBKrR',
			},
			{ name: 'theme-color', content: '#303275' },
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{ rel: 'icon', type: 'image/png', href: '/favicon.png' },
			{ rel: 'canonical', href: 'https://quranuiren.kz/' },
			{
				rel: 'apple-touch-icon',
				sizes: '180x180',
				href: '/img/favicon/apple-touch-icon.png?v=dLXj2jBKrR',
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '32x32',
				href: '/img/favicon/favicon-32x32.png?v=dLXj2jBKrR',
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '194x194',
				href: '/img/favicon/favicon-194x194.png?v=dLXj2jBKrR',
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '192x192',
				href: '/img/favicon/android-chrome-192x192.png?v=dLXj2jBKrR',
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '16x16',
				href: '/img/favicon/favicon-16x16.png?v=dLXj2jBKrR',
			},
			{ rel: 'manifest', href: '/img/favicon/site.webmanifest?v=dLXj2jBKrR' },
			{
				rel: 'mask-icon',
				href: '/img/favicon/safari-pinned-tab.svg?v=dLXj2jBKrR',
				color: '#303275',
			},
			{ rel: 'shortcut icon', href: '/img/favicon/favicon.ico?v=dLXj2jBKrR' },
		],
		// script: [{ src: 'http://192.168.1.200:8098' }],
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#093', height: '5px' },
	/*
	 ** Global CSS
	 */
	css: ['~/assets/common.styl'],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: [
		{ src: '~/plugins/vuex-cache.js', ssr: false },
		'~/plugins/imlazy.js',
	],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [
		// Doc: https://github.com/nuxt-community/eslint-module
		'@nuxtjs/eslint-module',
	],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		// https://pwa.nuxtjs.org/
		'@nuxtjs/pwa',
		'@nuxtjs/svg-sprite',
		'@nuxtjs/style-resources',
	],
	svgSprite: {
		input: '~/assets/svg-icons/',
		output: '~/assets/svg-sprite/',
	},
	styleResources: {
		// stylus: ['~/assets/vars.styl'],
	},
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {},
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, { isDev, isClient }) {
			if (isDev) {
				config.devtool = isClient ? 'source-map' : 'inline-source-map'
			}
			;[]
				.concat(
					...config.module.rules
						.find(e => e.test.toString().match(/\.styl/))
						.oneOf.map(e => e.use.filter(e => e.loader === 'stylus-loader'))
				)
				.forEach(stylus => {
					Object.assign(stylus.options, {
						// preferPathResolver: 'webpack',
						sourceMap: true,
						stylusOptions: {
							// use: ['nib'],
							// include all styl files from folder
							include: ['~assets/common.styl'],
							import: [
								'~assets/vars.styl', // your main styl file
							],
						},
					})
					// console.log(stylus)
				})
		},
	},
	pwa: {
		manifest: {
			name: 'Құран Әлiппесi',
			short_name: 'Құран Әлiппесi',
			description: 'Жаңа әдiстеме: 20 дәрic Құран оқып үйрену оңай',
			lang: 'kz',
			screenshots: [],
			display: 'standalone',
			// orientation: 'portrait',
			background_color: '#ffffff',
			theme_color: '#fff',
			generated: '',
		},
		meta: {
			mobileAppIOS: true,
			name: 'Құран Әліппесі',
			author: 'Asyl Arna',
			lang: 'kz',
		},
		workbox: {
			// список файлов для принудительного кеширования
		},
	},
	generate: {
		routes() {
			return Array(20)
				.fill(0)
				.map((_, i) => `/lesson/${String(i + 1).padStart(2, '0')}`)
		},
	},
}
