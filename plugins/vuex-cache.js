import createVuexCache from 'vuex-cache'

export default ({ store }) => {
	const options = {
		timeout: 7200, // Equal to 2 hours in milliseconds = 2 * 60 * 60 * 1000
	}

	const setupVuexCache = createVuexCache(options)

	window.onNuxtReady(() => setupVuexCache(store))
}
