// автоподключение компонентов
import Vue from 'vue'
import Vue2TouchEvents from 'vue2-touch-events'
;['FDummy', 'FSvg'].forEach(name =>
	// eslint-disable-next-line
	Vue.component(name, () => import(`~/components/${name}.vue`))
)

// поддержка перелистывания (swips)
Vue.use(Vue2TouchEvents, {
	swipeTolerance: 90,
})

// полезняшки для отладки
// просто лог
// eslint-disable-next-line no-console
Vue.prototype.$log = console.log.bind(console)

// Эмулятор вызовов АПИ или шипатишный фирмовый лог
Vue.prototype.$hi = function (data = {}, comment = document.title) {
	const conStyle = `
    font:16px bold;
    min-width: 100px;
    line-height: 48px;
    background:url(${location.origin}/icon.png) no-repeat left top;
    background-size: 48px;
    padding-left: 52px;
    `

	// eslint-disable-next-line no-console
	console.log('%c%s\n%o', conStyle, comment, data)
}

// Referer - не то же самое, что History 😜🥴
Vue.mixin({
	beforeRouteEnter(to, from, next) {
		next(vm => {
			if (vm.$router) {
				vm.$router.referer = from
			}
		})
	},

	beforeRouteUpdate(to, from, next) {
		if (this.$router) {
			this.$router.referer = from
		}
		next()
	},
})
