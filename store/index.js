/**
 * Howler нампрямую меняет значения в хранилище, возникает ошибка. По-этому,
 * приходится подавлять ВСЕ ошибки в проекте. Это неправильно, но если shift()
 * можно обойти, например, вообще не менять объект внутри стейта, а менять
 * только индекс текущего звука, то проблему с Howler непонятно как решать,
 * т.к. даже имея в стейте только ссылку на очередь, при срабатывании события
 * `onend` звука, Howler меняет состояние объекта звука и Вуекс матерится -
 * хотя меняется лишь состояние звука -- поёт/не поёт 😢😭🤪
 */

export const strict = false

export const state = () => ({
	lessons: {},
	durations: {},
	showDrawer: false,
	audio: {
		current: undefined,
		queue: [],
	},
})

export const mutations = {
	SET_CURRENT_SND(state, snd) {
		state.currentSnd = snd
	},

	SET_AUDIO_QUEUE(state, queue) {
		state.audio.queue = queue.slice()
	},

	PLAY_NEXT_IN_QUEUE(state) {
		state.audio.current = state.audio.queue.shift()
		if (state.audio.current !== undefined) {
			state.audio.current.play()
		}
	},

	SHOW_DRAWER(state, isShown) {
		state.showDrawer = isShown
	},

	TOGGLE_DRAWER(state) {
		state.showDrawer = !state.showDrawer
	},

	SET_LESSONS(state, data) {
		return (state.lessons = data)
	},

	SET_DURATIONS(state, data) {
		return (state.durations = data)
	},
}

export const actions = {
	// action(store, payload)

	async FETCH_LESSONS({ commit }) {
		commit('SET_LESSONS', await fetch('/data/lessons.json').then(r => r.json()))
	},

	async FETCH_DURATIONS({ commit }) {
		commit(
			// eslint-disable-next-line prettier/prettier
			'SET_DURATIONS',
			await fetch('/data/durations.json').then(r => r.json())
		)
	},
}

export const getters = {
	isHowl(state) {
		return (
			state.audio.queue.length > 0 ||
			(state.audio.current !== undefined && state.audio.current.playing())
		)
	},

	isDrawerShown(state) {
		return state.showDrawer
	},

	LESSONS: state => state.lessons,
	DURATIONS: state => state.durations,

	lesson: state => no =>
		!Object.keys(state.lessons).length ? {} : state.lessons[no],

	duration: state => (lessonNo, cifir) =>
		!Object.keys(state.durations).length
			? {}
			: state.durations[lessonNo][cifir],
}
