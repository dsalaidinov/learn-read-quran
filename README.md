# https://quranuiren.kz | https://play.google.com/store/apps/details?id=kz.quranuiren.twa [Android]

> This application helps you learn to read the Holy Quran step by step in 20 lessons. Technologies used: vue, nuxt, pwa, pug, stylus

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
